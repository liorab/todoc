<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([[
            'name' => 'lior',
            'email' => 'lior@lior.com',
            'password' => Hash::make('12345678'),
            'created_at' => date('Y-m-d G:i:s'),
            ],
        [
            'name' => 'dorin',
            'email' => 'dorin@dorin.com',
            'password' => Hash::make('12345678'),
            'created_at' => date('Y-m-d G:i:s'),
        ],
        ]);
    }
}
