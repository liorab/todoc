@extends('layouts.app')
@section('content')

<h1>This is your todo list</h1>
<ul>

    @foreach($todos as $todo)
    <li>
    @if ($todo->status)
           <input type = 'checkbox' id ="{{$todo->id}}" checked>
       @else
           <input type = 'checkbox' id ="{{$todo->id}}">
       @endif
       @cannot('employee')

    <a href="{{route('todos.edit',$todo->id)}}"> @endcannot {{$todo->title}} </a>
    </li>
    </ul>
    <ul>
    @endforeach
    @cannot('employee')
    <a href="{{route('todos.create')}}">Create New todo </a>
    @endcannot
</ul>
<script>
       $(document).ready(function(){
           $(":checkbox").click(function(event){
               console.log(event.target.id)
               $.ajax({
                   url:  "{{url('todos')}}"+'/'+event.target.id,
                   dataType: 'json',
                   type: 'PUT' ,
                   contentType: 'application/json',
                   data: JSON.stringify({'status':event.target.checked, _token:'{{csrf_token()}}'}),
                   processData:false,
                   success: function( data){
                        console.log(JSON.stringify( data ));
                   },
                   error: function(errorThrown ){
                       console.log( errorThrown );
                   }
               });               
           });
       });
   </script>
@endsection
